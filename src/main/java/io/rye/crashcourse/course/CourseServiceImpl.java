package io.rye.crashcourse.course;

import io.rye.crashcourse.CommonHelper;
import io.rye.crashcourse.course.data.CourseAddDTO;
import io.rye.crashcourse.course.data.CourseDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

/**
 * Created on 2022-05-01
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Service
public class CourseServiceImpl implements CourseService{

  private final ConversionService conversionService;
  private final CourseRepository courseRepository;
  private final CommonHelper commonHelper;

  public CourseServiceImpl(@Qualifier("mvcConversionService") ConversionService conversionService,
                           CourseRepository courseRepository,
                           CommonHelper commonHelper) {
    this.conversionService = conversionService;
    this.courseRepository = courseRepository;
    this.commonHelper = commonHelper;
  }

  @Override
  public CourseDTO addCourse(CourseAddDTO request) {
    Course course = conversionService.convert(request, Course.class);
    if (course == null) {
      throw new IllegalStateException("Conversion error!");
    }

    Course courseResult = courseRepository.save(course);
    return conversionService.convert(courseResult, CourseDTO.class);
  }

  @Override
  public List<CourseDTO> getAllCourses() {
    List<Course> courseList = courseRepository.findAllByOrderByCreatedAtDesc();

    TypeDescriptor srcType = commonHelper.typeDescriptorList(Course.class);
    TypeDescriptor targetType = commonHelper.typeDescriptorList(CourseDTO.class);

    return (List<CourseDTO>) conversionService.convert(courseList, srcType, targetType);
  }

  @Override
  public CourseDTO updateCourse(Long id, CourseAddDTO request) {
    Course course = courseRepository
      .findById(id)
      .orElseThrow(() -> new IllegalStateException(String.format("Course ID: %s not found", id)));

    Course newCourse = conversionService.convert(request, Course.class);
    if (newCourse == null) {
      throw new IllegalStateException("Conversion error!");
    }
    newCourse.setId(course.getId());
    newCourse.setCreatedAt(course.getCreatedAt());
    newCourse.setUpdatedAt(LocalDateTime.now(ZoneId.of("Asia/Manila")));

    Course courseResult = courseRepository.save(newCourse);
    return conversionService.convert(courseResult, CourseDTO.class);
  }

  @Override
  public void deleteCourseById(Long id) {
    Course course = courseRepository
      .findById(id)
      .orElseThrow(() -> new IllegalStateException(String.format("Course ID: %s not found", id)));

    courseRepository.deleteById(id);
  }

}
