package io.rye.crashcourse.course;

import io.rye.crashcourse.enrollment.Enrollment;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static javax.persistence.CascadeType.*;
import static javax.persistence.GenerationType.SEQUENCE;

/**
 * Created on 2022-05-01
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Builder
@AllArgsConstructor
@Entity(name = "Course")
@Table(
  name = "course",
  uniqueConstraints = {
    @UniqueConstraint(name = "course_name_unique", columnNames = "name")
  }
)
public class Course {

  @Id
  @Column(name = "id", updatable = false, nullable = false)
  @SequenceGenerator(
    name = "course_id_sequence",
    sequenceName = "course_id_sequence",
    allocationSize = 1
  )
  @GeneratedValue(strategy = SEQUENCE, generator = "course_id_sequence")
  private Long id;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "department", nullable = false)
  private String department;

  @OneToMany(cascade = ALL, mappedBy = "course")
  private List<Enrollment> enrollments = new ArrayList<>();

  @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private LocalDateTime createdAt;

  @Column(name = "updated_at", columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private LocalDateTime updatedAt;

  public void addEnrollment(Enrollment enrollment) {
    if (!enrollments.contains(enrollment)) {
      enrollments.add(enrollment);
    }
  }

  public void removeEnrollment(Enrollment enrollment) {
    enrollments.remove(enrollment);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Course course = (Course) o;
    return id != null && Objects.equals(id, course.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

}
