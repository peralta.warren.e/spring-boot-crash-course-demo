package io.rye.crashcourse.course.converter;

import io.rye.crashcourse.course.Course;
import io.rye.crashcourse.course.data.CourseDTO;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created on 2022-05-01
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Component
@AllArgsConstructor
public class CourseResponseToDTOConverter implements Converter<Course, CourseDTO> {

  @Override
  public CourseDTO convert(Course src) {
    return new CourseDTO(
      src.getId(),
      src.getName(),
      src.getDepartment(),
      src.getCreatedAt(),
      src.getUpdatedAt()
    );
  }

}
