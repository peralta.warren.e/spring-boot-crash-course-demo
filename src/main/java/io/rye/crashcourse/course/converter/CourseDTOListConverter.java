package io.rye.crashcourse.course.converter;

import io.rye.crashcourse.course.Course;
import io.rye.crashcourse.course.data.CourseDTO;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 2022-05-01
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Component
@AllArgsConstructor
public class CourseDTOListConverter implements Converter<List<Course>, List<CourseDTO>> {

  @Override
  public List<CourseDTO> convert(List<Course> src) {
    return src.stream().map(
      s -> new CourseDTO(
        s.getId(),
        s.getName(),
        s.getDepartment(),
        s.getCreatedAt(),
        s.getUpdatedAt()
      )
    ).collect(Collectors.toList());
  }

}
