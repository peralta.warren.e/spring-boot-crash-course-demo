package io.rye.crashcourse;

import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created on 2022-04-30
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Component
public class CommonHelper {

  public TypeDescriptor typeDescriptorList(Class<?> type) {
    return TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(type));
  }

}
