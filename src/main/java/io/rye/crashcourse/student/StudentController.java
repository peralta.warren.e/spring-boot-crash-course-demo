package io.rye.crashcourse.student;

import io.rye.crashcourse.enrollment.data.EnrollmentAddDTO;
import io.rye.crashcourse.student.data.StudentAddDTO;
import io.rye.crashcourse.student.data.StudentDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

/**
 * Created on 2022-04-30
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@RestController
@RequestMapping(path = "/api/v1/students", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
@AllArgsConstructor
public class StudentController {

  private final StudentService studentService;

  @PostMapping(consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
  public ResponseEntity<StudentDTO> addStudent(@Valid @RequestBody StudentAddDTO request) {
    StudentDTO response = studentService.addStudent(request);
    return new ResponseEntity<>(response, CREATED);
  }

  @GetMapping
  public ResponseEntity<List<StudentDTO>> getAllStudents() {
    List<StudentDTO> studentList = studentService.getAllStudents();
    return new ResponseEntity<>(studentList, OK);
  }

  @PutMapping(path = "{id}")
  public ResponseEntity<StudentDTO> updateStudent(@PathVariable("id") Long id,
                                                  @Valid @RequestBody StudentAddDTO request) {
    StudentDTO response = studentService.updateStudent(id, request);
    return new ResponseEntity<>(response, OK);
  }

  @DeleteMapping(path = "{id}")
  public ResponseEntity<?> deleteStudent(@PathVariable("id") Long id) {
    studentService.deleteStudentById(id);
    return new ResponseEntity<>(OK);
  }

  @PostMapping(path = "{id}/enrollments")
  public ResponseEntity<String> addStudentEnrollment(@PathVariable("id") Long id,
                                                     @Valid @RequestBody EnrollmentAddDTO request) {
    String response = studentService.addStudentEnrollment(id, request);
    return new ResponseEntity<>(response, OK);
  }

}
