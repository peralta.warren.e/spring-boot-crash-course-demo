package io.rye.crashcourse.student.converter;

import io.rye.crashcourse.student.Student;
import io.rye.crashcourse.student.data.StudentDTO;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 2022-04-30
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Component
@AllArgsConstructor
public class StudentDTOListConverter implements Converter<List<Student>, List<StudentDTO>> {

  @Override
  public List<StudentDTO> convert(List<Student> src) {
    return src.stream().map(
      s -> new StudentDTO(
        s.getId(),
        s.getFirstName(),
        s.getLastName(),
        s.getBirthdate(),
        s.getPersonalEmail(),
        s.getCreatedAt(),
        s.getUpdatedAt()
      )
    ).collect(Collectors.toList());
  }

}
