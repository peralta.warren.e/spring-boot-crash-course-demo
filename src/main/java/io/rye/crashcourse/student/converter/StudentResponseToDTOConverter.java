package io.rye.crashcourse.student.converter;

import io.rye.crashcourse.student.Student;
import io.rye.crashcourse.student.data.StudentDTO;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created on 2022-04-30
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Component
@AllArgsConstructor
public class StudentResponseToDTOConverter implements Converter<Student, StudentDTO> {

  @Override
  public StudentDTO convert(Student src) {
    return new StudentDTO(
      src.getId(),
      src.getFirstName(),
      src.getLastName(),
      src.getBirthdate(),
      src.getPersonalEmail(),
      src.getCreatedAt(),
      src.getUpdatedAt()
    );
  }

}
