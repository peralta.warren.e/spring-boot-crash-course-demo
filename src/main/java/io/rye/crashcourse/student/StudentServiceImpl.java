package io.rye.crashcourse.student;

import io.rye.crashcourse.CommonHelper;
import io.rye.crashcourse.course.Course;
import io.rye.crashcourse.course.CourseRepository;
import io.rye.crashcourse.enrollment.Enrollment;
import io.rye.crashcourse.enrollment.EnrollmentId;
import io.rye.crashcourse.enrollment.data.EnrollmentAddDTO;
import io.rye.crashcourse.student.data.StudentAddDTO;
import io.rye.crashcourse.student.data.StudentDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

/**
 * Created on 2022-04-30
 *
 * @author Ryan Jan Borja
 * rrborja@region1.dost.gov.ph /
 * ryanjan18@outlook.ph
 */

@Service
public class StudentServiceImpl implements StudentService {

  private final ConversionService conversionService;

  private final StudentRepository studentRepository;
  private final CourseRepository courseRepository;

  private final CommonHelper commonHelper;

  public StudentServiceImpl(@Qualifier("mvcConversionService") ConversionService conversionService,
                            StudentRepository studentRepository,
                            CourseRepository courseRepository,
                            CommonHelper commonHelper) {
    this.conversionService = conversionService;
    this.studentRepository = studentRepository;
    this.courseRepository = courseRepository;
    this.commonHelper = commonHelper;
  }

  @Override
  public StudentDTO addStudent(StudentAddDTO request) {
    Student student = conversionService.convert(request, Student.class);
    if (student == null) {
      throw new IllegalStateException("Conversion error!");
    }

    Student studentResult = studentRepository.save(student);
    return conversionService.convert(studentResult, StudentDTO.class);
  }

  @Override
  public List<StudentDTO> getAllStudents() {
    List<Student> studentList = studentRepository.findAllByOrderByCreatedAtDesc();

    TypeDescriptor srcType = commonHelper.typeDescriptorList(Student.class);
    TypeDescriptor targetType = commonHelper.typeDescriptorList(StudentDTO.class);

    return (List<StudentDTO>) conversionService.convert(studentList, srcType, targetType);
  }

  @Override
  public StudentDTO updateStudent(Long id, StudentAddDTO request) {
    Student student = studentRepository
      .findById(id)
      .orElseThrow(() -> new IllegalStateException(String.format("Student ID: %s not found", id)));

    Student newStudent = conversionService.convert(request, Student.class);
    if (newStudent == null) {
      throw new IllegalStateException("Conversion error!");
    }
    newStudent.setId(student.getId());
    newStudent.setCreatedAt(student.getCreatedAt());
    newStudent.setUpdatedAt(LocalDateTime.now(ZoneId.of("Asia/Manila")));

    Student studentResult = studentRepository.save(newStudent);
    return conversionService.convert(studentResult, StudentDTO.class);
  }

  @Override
  public void deleteStudentById(Long id) {
    studentRepository
      .findById(id)
      .orElseThrow(() -> new IllegalStateException(String.format("Student ID: %s not found", id)));

    studentRepository.deleteById(id);
  }

  @Override
  public String addStudentEnrollment(Long id, EnrollmentAddDTO request) {
    Student student = studentRepository
      .findById(id)
      .orElseThrow(() -> new IllegalStateException(String.format("Student ID: %s not found", id)));

    Course course = courseRepository
      .findById(request.getCourseId())
      .orElseThrow(() -> new IllegalStateException(String.format("Course ID: %s not found", id)));

    Enrollment enrollment = new Enrollment(
      new EnrollmentId(student.getId(), course.getId()),
      student,
      course,
      LocalDateTime.now(ZoneId.of("Asia/Manila"))
    );

    student.addEnrollment(enrollment);

    studentRepository.save(student);

    return String.format(
      "%s %s was successfully enrolled to %s",
      student.getFirstName(),
      student.getLastName(),
      course.getName()
    );
  }

}
